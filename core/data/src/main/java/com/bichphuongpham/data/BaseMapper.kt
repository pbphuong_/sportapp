package com.bichphuongpham.data

abstract class BaseMapper<T1, T2> {

    abstract fun map(from: T1): T2
}