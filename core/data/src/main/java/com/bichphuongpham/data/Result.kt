package com.bichphuongpham.data

/**
 * Data wrapper around responses to handle success as well as failure states.
 */
sealed class Result<out T : Any> {

    data class Success<out T : Any>(val data: T) : Result<T>()

    data class Failure(val errorType: ErrorType) : Result<Nothing>()

    data class FailureWithData<out T : Any>(val data: T, val errorType: ErrorType) : Result<T>()

    fun successOrError(onSuccess: (T) -> Any, onFailure: (ErrorType) -> Any): Any =
        when (this) {
            is Success -> onSuccess(data)
            is Failure -> onFailure(errorType)
            is FailureWithData -> {
                onSuccess(data)
                onFailure(errorType)
            }
        }
}