package com.bichphuongpham.data

sealed class ErrorType {

    object NetworkConnectionError : ErrorType()

    object ServerError : ErrorType()

    object NoDataError : ErrorType()
}