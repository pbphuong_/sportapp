plugins {
    id(Plugin.ANDROID_LIBRARY)
    id(Plugin.KOTLIN_ANDROID)
    kotlin(Plugin.KAPT)
}

android {
    compileSdkVersion(BuildConfig.TARGET_SDK)

    defaultConfig {
        minSdkVersion(BuildConfig.MIN_SDK)
        targetSdkVersion(BuildConfig.TARGET_SDK)
        versionCode = BuildConfig.VERSION_CODE
        versionName = BuildConfig.VERSION_NAME
    }

    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Navigation
    api(project(CoreModuleDependency.CORE_NAVIGATION))

    // Koin
    api(LibraryDependency.KOIN_VIEWMODEL)

    // Lifecycle
    implementation(LibraryDependency.APPCOMPAT)

    // UI
    implementation(LibraryDependency.ANKO_SDK)
    implementation(LibraryDependency.CONSTRAINT_LAYOUT)
    api(LibraryDependency.MATERIAL_DESIGN)
}