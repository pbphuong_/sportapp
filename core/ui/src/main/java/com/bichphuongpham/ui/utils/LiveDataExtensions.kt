package com.bichphuongpham.ui.utils

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

/**
 * Listen to a LiveData object and trigger an action on the object's changes.
 *
 * @param T the data type of the object
 * @param lifecycleOwner any lifecycle owner
 * @param doOnObserve a method that will be triggered on observed changes
 */
fun <T> LiveData<T>.observe(lifecycleOwner: LifecycleOwner, doOnObserve: (T?) -> (Unit)) {
    val observer: Observer<T> = Observer { value -> doOnObserve(value) }
    this.observe(lifecycleOwner, observer)
}

/**
 * Listen to a LiveData object and trigger an action on the object's changes.
 *
 * @param T the data type of the object
 * @param fragment the fragment as a lifecycle owner
 * @param doOnObserve a method that will be triggered on observed changes
 */
fun <T> LiveData<T>.observe(fragment: Fragment, doOnObserve: (T?) -> (Unit)) {
    observe(fragment.viewLifecycleOwner, doOnObserve)
}

/**
 * Listen to a LiveData object and trigger an action on the object's nonnull changes only.
 *
 * @param T the data type of the object
 * @param lifecycleOwner any lifecycle owner
 * @param doOnObserve a method that will be triggered on observed changes
 */
fun <T> LiveData<T>.nonNullObserve(owner: LifecycleOwner, observer: (t: T) -> Unit) {
    this.observe(owner, Observer {
        it?.let(observer)
    })
}