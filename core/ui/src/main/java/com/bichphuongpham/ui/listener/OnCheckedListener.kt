package com.bichphuongpham.ui.listener

interface OnCheckedListener {

    fun onCheckedChange(isChecked: Boolean)
}