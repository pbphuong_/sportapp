package com.bichphuongpham.ui.lifecycle

import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.bichphuongpham.navigation.NavigationCommand
import com.bichphuongpham.navigation.Navigator
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 * This class provides common base functions for ViewModels.
 */
abstract class BaseViewModel : ViewModel(), Navigator {

    private var args = Bundle()

    val navigationCommands by lazy { SingleLiveEvent<NavigationCommand>() }

    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
    }

    open fun loadData() {}

    open fun setArguments(arguments: Bundle) {
        args = arguments
    }

    override fun navigate(directions: NavDirections) {
        navigationCommands.postValue(NavigationCommand.To(directions))
    }

    override fun navigateBack() {
        navigationCommands.postValue(NavigationCommand.Back)
    }

    fun launch(block: suspend CoroutineScope.() -> Unit) =
        viewModelScope.launch(coroutineExceptionHandler) {
            block()
        }
}