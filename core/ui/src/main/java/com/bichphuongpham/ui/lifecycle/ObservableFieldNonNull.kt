package com.bichphuongpham.ui.lifecycle

import androidx.databinding.Observable
import androidx.databinding.ObservableField

class ObservableFieldNonNull<T : Any>(value: T, vararg dependencies: Observable) : ObservableField<T>(*dependencies) {

    init {
        set(value)
    }

    override fun get(): T = super.get()!!

    @Suppress("RedundantOverride")
    override fun set(value: T) = super.set(value)    // Only allow non-null value
}