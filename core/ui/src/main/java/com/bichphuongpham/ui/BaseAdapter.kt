package com.bichphuongpham.ui

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * This adapter is for binding the data with its view.
 */
abstract class BaseAdapter<T>(
    protected val data: MutableList<T> = mutableListOf()
) : RecyclerView.Adapter<BaseAdapter.ViewHolder>() {

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder

    /**
     * Bind the data.
     *
     * @param holder the view
     * @param position item's index in the list
     */
    abstract override fun onBindViewHolder(holder: ViewHolder, position: Int)

    override fun getItemCount() = data.size

    open fun setData(newData: MutableList<T>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }
}