package com.bichphuongpham.ui.lifecycle

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * This class provides common base functions for application activities.
 */
abstract class BaseActivity : AppCompatActivity() {

    /**
     * The layout id of the activity.
     */
    protected abstract val layoutId: Int

    /**
     * Initialize the UI layout.
     */
    open fun initUi() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        initUi()
    }
}