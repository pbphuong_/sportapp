package com.bichphuongpham.ui.utils

import android.text.TextUtils
import android.view.View
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.NumberPicker
import androidx.core.widget.doOnTextChanged
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bichphuongpham.ui.BaseAdapter
import com.bichphuongpham.ui.listener.OnCheckedListener
import com.bichphuongpham.ui.listener.OnValueChangedListener
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("isVisible")
fun setIsVisible(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.GONE
}

@BindingAdapter("isInvisible")
fun setIsInvisible(view: View, isInvisible: Boolean) {
    view.visibility = if (isInvisible) View.INVISIBLE else View.VISIBLE
}

@BindingAdapter("bindData")
fun <T> setBindData(view: RecyclerView, data: List<T>?) {
    if (view.adapter is BaseAdapter<*>) {
        (view.adapter as BaseAdapter<T>).setData(data?.toMutableList() ?: mutableListOf())
    }
}

@BindingAdapter("onCheckedChangeListener")
fun setOnCheckedChangeListener(view: CompoundButton, onCheckedChangeListener: OnCheckedListener) {
    view.setOnCheckedChangeListener { _, isChecked ->
        onCheckedChangeListener.onCheckedChange(isChecked)
    }
}

@BindingAdapter("onValueChangeListener")
fun setOnValueChangeListener(view: NumberPicker, onValueChangedListener: OnValueChangedListener) {
    view.setOnValueChangedListener { _, _, newVal ->
        onValueChangedListener.onValueChanged(newVal)
    }
}

@BindingAdapter("removeErrorOnEdittextChange")
fun <T> setRemoveErrorOnEdittextChange(view: TextInputLayout, editText: EditText) {
    editText.doOnTextChanged { text, _, _, _ ->
        if (text?.isNotBlank() == true) {
            // If the text input is changed, remove the error message
            if (!TextUtils.isEmpty(view.error)) {
                view.error = null
            }
        }
    }
}