package com.bichphuongpham.ui.utils

import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.NumberPicker
import androidx.annotation.LayoutRes
import androidx.core.view.children
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import org.jetbrains.anko.layoutInflater

/**
 * Method for inflating root layout resources of custom views.
 *
 * @param layoutRes the custom view layout id
 */
fun ViewGroup.inflateCustomViewContent(@LayoutRes layoutRes: Int) {
    this.context.layoutInflater.inflate(layoutRes, this, true)
}

/**
 * This method will remove default input filter so the initial value can be formatted.
 */
fun NumberPicker.removeDefaultFilters() {
    children.iterator().forEach {
        if (it is EditText) it.filters = arrayOfNulls(0)
    }
}

/**
 * Show a snackbar view with a corresponding message.
 */
fun Fragment.showSnackbar(container: View, messageRedId: Int, duration: Int, anchorView: View? = null) {
    Snackbar.make(container, getString(messageRedId), duration).apply {
        this.anchorView = anchorView
    }.show()
}