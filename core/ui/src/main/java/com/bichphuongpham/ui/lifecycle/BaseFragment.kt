package com.bichphuongpham.ui.lifecycle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bichphuongpham.navigation.NavigationCommand
import com.bichphuongpham.ui.utils.observe

/**
 * This class provides common base functions for fragments.
 *
 * @param VM ViewModel class
 * @param VB DataBinding class
 */
abstract class BaseFragment<VM : BaseViewModel, VB : ViewDataBinding> : Fragment() {

    lateinit var binding: VB
    lateinit var viewModel: VM

    protected abstract val layoutId: Int

    /**
     * Initialize the DataBinding between the layout and the data holder such as ViewModel.
     */
    abstract fun initBindingDataSources()

    abstract fun initViewModel()

    open fun initUi() {}

    open fun initObservables() {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        initViewModel()

        // Save and load arguments
        if (savedInstanceState == null) {
            arguments?.let {
                viewModel.setArguments(it)
                viewModel.loadData()
            }
        }

        val view = inflater.inflate(layoutId, container, false)
        binding = DataBindingUtil.bind(view) ?: return view         // Inflates binding
        binding.lifecycleOwner = this                               // Sets a lifecycle observer - for LiveData observers in the XML
        initBindingDataSources()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Otherwise the animateLayoutChanges would interfere with ViewPager2 scrolling
        (view as? ViewGroup)?.layoutTransition?.setAnimateParentHierarchy(false)

        super.onViewCreated(view, savedInstanceState)
        listenToNavigationCommands()
        initObservables()
        initUi()
    }

    /**
     * Observe on navigation commands to navigate to corresponding destinations.
     */
    private fun listenToNavigationCommands() {
        viewModel.navigationCommands.observe(this) { command ->
            val navController = findNavController()

            navController.apply {
                when (command) {
                    is NavigationCommand.To -> navigate(command.directions)
                    is NavigationCommand.Back -> navigateUp()
                    is NavigationCommand.BackTo -> popBackStack(command.destinationId, false)
                }
            }
        }
    }
}