package com.bichphuongpham.ui.listener

interface OnValueChangedListener {

    fun onValueChanged(value: Int)
}