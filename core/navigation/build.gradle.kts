plugins {
    id(Plugin.ANDROID_LIBRARY)
    id(Plugin.KOTLIN_ANDROID)
    kotlin(Plugin.KAPT)
}

android {
    compileSdkVersion(BuildConfig.TARGET_SDK)

    defaultConfig {
        minSdkVersion(BuildConfig.MIN_SDK)
        targetSdkVersion(BuildConfig.TARGET_SDK)
        versionCode = BuildConfig.VERSION_CODE
        versionName = BuildConfig.VERSION_NAME
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Navigation
    api(LibraryDependency.NAVIGATION_FRAGMENT)
    implementation(LibraryDependency.NAVIGATION_UI)
}