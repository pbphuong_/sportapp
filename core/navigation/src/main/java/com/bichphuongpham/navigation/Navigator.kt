package com.bichphuongpham.navigation

import androidx.navigation.NavDirections

interface Navigator {

    fun navigate(directions: NavDirections)

    fun navigateBack()
}