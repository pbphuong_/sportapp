package com.bichphuongpham.navigation

import androidx.navigation.NavDirections

/**
 * This class represents messages processed by the @[Navigator].
 * They determine the next destination.
 */
sealed class NavigationCommand {

    data class To(val directions: NavDirections) : NavigationCommand()

    object Back : NavigationCommand()

    data class BackTo(val destinationId: Int) : NavigationCommand()
}