package com.bichphuongpham.network

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitBuilder {

    fun buildRetrofit(baseUrl: String, client: OkHttpClient): Retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl(baseUrl)
        .addConverterFactory(MoshiConverterFactory.create().asLenient())    // Forgiving parser
        .build()

    fun buildClient(interceptors: List<Interceptor> = listOf()): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()

        // Add each interceptor to the client
        interceptors.forEach { clientBuilder.addInterceptor(it) }

        // Add logging only in debug build
        if (BuildConfig.DEBUG) {
            clientBuilder.addInterceptor(buildLoggingInterceptor())
        }

        // Configure timeouts
        clientBuilder
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(30, TimeUnit.SECONDS)

        return clientBuilder.build()
    }

    private fun buildLoggingInterceptor() = HttpLoggingInterceptor().apply {
        // Log the body of the request, give detailed report of the sent and received data
        level = HttpLoggingInterceptor.Level.BODY
    }
}