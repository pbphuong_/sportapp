package com.bichphuongpham.network.interceptors

import com.bichphuongpham.network.NetworkHandler
import com.bichphuongpham.network.exception.NoInternetConnectionException
import okhttp3.Interceptor
import okhttp3.Response

class NetworkInterceptor(private val connectionHandler: NetworkHandler) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return when (connectionHandler.isNetworkAvailable()) {
            true -> chain.proceed(chain.request())
            false -> throw NoInternetConnectionException()
        }
    }
}