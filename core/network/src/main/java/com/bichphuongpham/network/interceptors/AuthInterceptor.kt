package com.bichphuongpham.network.interceptors

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(
    private val headers: HashMap<String, String> = hashMapOf(),
    private val queryParams: HashMap<String, String> = hashMapOf()
) :
    Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        // Add query parameters to the endpoint of the original request
        val urlBuilder = originalRequest.url.newBuilder()
        queryParams.forEach { (key, value) -> urlBuilder.addQueryParameter(key, value) }
        val newUrl = urlBuilder.build()

        val requestWithNewQuery = originalRequest.newBuilder().url(newUrl).build()

        // Add headers to the request
        val headerBuilder = requestWithNewQuery.newBuilder()
        headers.forEach { (key, value) -> headerBuilder.addHeader(key, value) }
        val newRequest = headerBuilder.build()

        return chain.proceed(newRequest)
    }
}