package com.bichphuongpham.network.exception

import java.io.IOException

class NoInternetConnectionException : IOException()