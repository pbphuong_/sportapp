object CoreDependency {
    object Version {
        const val KOTLIN = "1.3.72"
    }

    const val KOTLIN = "org.jetbrains.kotlin:kotlin-stdlib:${Version.KOTLIN}"
}