object Plugin {

    const val KAPT = "kapt"
    const val ANDROID_APPLICATION = "com.android.application"
    const val ANDROID_LIBRARY = "com.android.library"
    const val KOTLIN_ANDROID = "kotlin-android"
    const val KOTLIN_EXTENSIONS = "kotlin-android-extensions"
    const val FABRIC = "io.fabric"
}