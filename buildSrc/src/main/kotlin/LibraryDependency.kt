object LibraryDependency {
    object Version {
        const val APPCOMPAT = "1.3.0-beta01"
        const val LIVEDATA = "2.3.0"

        const val GRADLE = "4.2.0-beta04"
        const val FABRIC = "1.31.2"

        const val NAVIGATION = "2.3.0"

        const val KOIN = "2.2.2"

        const val MATERIAL_DESIGN = "1.3.0"
        const val CONSTRAINT_LAYOUT = "2.1.0-beta01"
        const val ANKO = "0.10.8"
        const val GLIDE = "4.11.0"

        const val TIMBER = "4.7.1"

        const val RETROFIT = "2.9.0"
        const val OKHTTP = "4.8.1"

        const val MOSHI = "1.11.0"
        const val MOSHI_RETROFIT_CONVERTER = "2.4.0"
        const val MOSHI_ADAPTER = "1.6.0"

        const val ROOM = "2.2.6"
    }

    // Lifecycle dependencies
    const val APPCOMPAT = "androidx.appcompat:appcompat:${Version.APPCOMPAT}"
    const val LIVEDATA = "androidx.lifecycle:lifecycle-livedata-ktx:${Version.LIVEDATA}"

    // Gradle dependencies
    const val GRADLE = "com.android.tools.build:gradle:${Version.GRADLE}"
    const val GRADLE_KOTLIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${CoreDependency.Version.KOTLIN}"
    const val FABRIC = "io.fabric.tools:gradle:${Version.FABRIC}"

    // Navigation
    const val NAVIGATION_FRAGMENT = "androidx.navigation:navigation-fragment-ktx:${Version.NAVIGATION}"
    const val NAVIGATION_UI = "androidx.navigation:navigation-ui-ktx:${Version.NAVIGATION}"

    // Koin
    const val KOIN_VIEWMODEL = "org.koin:koin-androidx-viewmodel:${Version.KOIN}"

    // UI
    const val MATERIAL_DESIGN = "com.google.android.material:material:${Version.MATERIAL_DESIGN}"
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${Version.CONSTRAINT_LAYOUT}"
    const val ANKO_SDK = "org.jetbrains.anko:anko-sdk25:${Version.ANKO}"
    const val GLIDE = "com.github.bumptech.glide:glide:${Version.GLIDE}"

    // Debugging
    const val TIMBER = "com.jakewharton.timber:timber:${Version.TIMBER}"

    // Networking
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${Version.RETROFIT}"
    const val OKHTTP = "com.squareup.okhttp3:okhttp:${Version.OKHTTP}"
    const val OKHTTP_LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:${Version.OKHTTP}"

    // Serialization
    const val MOSHI = "com.squareup.moshi:moshi:${Version.MOSHI}"
    const val MOSHI_RETROFIT_CONVERTER = "com.squareup.retrofit2:converter-moshi:${Version.MOSHI_RETROFIT_CONVERTER}"
    const val MOSHI_ADAPTER = "com.squareup.moshi:moshi-kotlin-codegen:${Version.MOSHI_ADAPTER}"

    // Room
    const val ROOM = "androidx.room:room-runtime:${Version.ROOM}"
    const val ROOM_COROUTINES = "androidx.room:room-ktx:${Version.ROOM}"
    const val ROOM_COMPILER = "androidx.room:room-compiler:${Version.ROOM}"
}