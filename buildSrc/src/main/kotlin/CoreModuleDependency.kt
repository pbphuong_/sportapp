object CoreModuleDependency {
    const val CORE_UI = ":core:ui"
    const val CORE_NAVIGATION = ":core:navigation"
    const val CORE_DATA = ":core:data"
    const val CORE_NETWORK = ":core:network"
}