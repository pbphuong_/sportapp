object MavenRepository {
    const val JITPACK = "https://www.jitpack.io"
    const val FABRIC = "https://maven.fabric.io/public"
}