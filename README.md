# SportApp

## Architecture

On the outside, the project is separated by feature into core (common) and feature modules. Each module is then divided into ui, domain and data layers according to the Clean architecture. The app was built with MVVM architecture in mind.  

<p align="center">
	<img src="documentation/images/module_graph.png" width="80%"/>
</p>

Brief description of each modules:

* **core::ui** - the base for all UI elements such as fragments and views
* **core::data** - contains base adapters and result wrappers needed when working with data
* **core::network** - defines the network client and interceptors such as logging, and a network connection interceptor
* **feature::record** - module with the main functionality logic

## Navigation
The navigation between the individual screens is implemented with the ViewPager and PagerAdapter. For the communication, I decided to create a SharedViewModel with the only functionality to trigger the ListingFragment to update its data when a new entry is submitted.

## Used libraries

* Koin - dependency injection
* Retrofit - networking
* OkHttp - network client and interceptors
* Moshi - serialization 
* Timber - logging 
* Material - UI 
* Firebase - remote storage
* Room - local storage

## How the app looks like on different resolutions
720x1280:
<p align="center">
  <img src="documentation/images/small_form.png" width="30%" />
  <img src="documentation/images/small_form_fill.png" width="30%" /> 
  <img src="documentation/images/small_listing.png" width="30%" /> 
</p>

1080x1920:
<p align="center">
  <img src="documentation/images/medium_form.png" width="30%" />
  <img src="documentation/images/medium_form_fill.png" width="30%" /> 
  <img src="documentation/images/medium_listing.png" width="30%" /> 
</p>

1440x2560:
<p align="center">
  <img src="documentation/images/large_form.png" width="30%" />
  <img src="documentation/images/large_form_fill.png" width="30%" /> 
  <img src="documentation/images/large_listing.png" width="30%" /> 
</p>

## How the different UI states were handled
Error states:
<p align="center">
  <img src="documentation/images/state_form_empty.png" width="30%" />
  <img src="documentation/images/state_no_internet_form.png" width="30%" /> 
  <img src="documentation/images/state_no_internet_listing.png" width="30%" />
</p>

Filter and success state:
<p align="center">
  <img src="documentation/images/state_filter.png" width="30%" />
  <img src="documentation/images/state_saved_success.png" width="30%" /> 
</p>

Landscape:
<p align="center">
	<img src="documentation/images/landscape_form_1.png" width="80%"/>
</p>

<p align="center">
	<img src="documentation/images/landscape_form_2.png" width="80%"/>
</p>

<p align="center">
	<img src="documentation/images/landscape_listing.png" width="80%"/>
</p>