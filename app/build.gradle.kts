plugins {
    id(Plugin.ANDROID_APPLICATION)
    id(Plugin.KOTLIN_ANDROID)
    kotlin(Plugin.KAPT)
}

android {
    compileSdkVersion(BuildConfig.TARGET_SDK)

    defaultConfig {
        applicationId = BuildConfig.APPLICATION_ID

        minSdkVersion(BuildConfig.MIN_SDK)
        targetSdkVersion(BuildConfig.TARGET_SDK)
        versionCode = BuildConfig.VERSION_CODE
        versionName = BuildConfig.VERSION_NAME

        multiDexEnabled = true      // If false, there is an R8 native error during compilation
    }

    // Without these lines the build would crash for older devices
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    buildTypes {
        getByName(BuildConfig.BUILD_TYPE_DEBUG) {
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    // Because dependant modules use dataBinding
    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Modules
    implementation(project(ModuleDependency.FEATURE_RECORD))

    // Koin
    implementation(LibraryDependency.KOIN_VIEWMODEL)

    // Debugging
    implementation(LibraryDependency.TIMBER)
}