package com.bichphuongpham.sport

import android.app.Application
import com.bichphuongpham.record.BuildConfig
import com.bichphuongpham.record.data.di.databaseModule
import com.bichphuongpham.record.data.di.networkModule
import com.bichphuongpham.record.ui.di.sportRecordsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * This class is for maintaining global application state.
 * Koin is initialized here.
 */
class Application : Application() {

    override fun onCreate() {
        super.onCreate()

        // Set logging only on debug build
        if (BuildConfig.DEBUG) {
            System.setProperty("kotlinx.coroutines.debug", "on")

            Timber.plant(object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement) =
                    String.format("Class:%s: Line: %s, ||", super.createStackElementTag(element), element.lineNumber)
            })
        }

        startKoin {
            androidLogger()
            androidContext(this@Application)
            loadKoinModules(listOf(sportRecordsModule, databaseModule, networkModule))
        }
    }
}