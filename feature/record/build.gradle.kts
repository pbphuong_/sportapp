import java.io.FileInputStream
import java.util.Properties

plugins {
    id(Plugin.ANDROID_LIBRARY)
    id(Plugin.KOTLIN_ANDROID)
    id(Plugin.KOTLIN_EXTENSIONS)    // Synthetic - is deprecated
    kotlin(Plugin.KAPT)
}

android {
    compileSdkVersion(BuildConfig.TARGET_SDK)

    defaultConfig {
        minSdkVersion(BuildConfig.MIN_SDK)
        targetSdkVersion(BuildConfig.TARGET_SDK)
        versionCode = BuildConfig.VERSION_CODE
        versionName = BuildConfig.VERSION_NAME

        buildConfigField("String", "BASE_URL", getProperty("BASE_URL"))
    }

    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Common modules
    api(project(CoreModuleDependency.CORE_UI))
    api(project(CoreModuleDependency.CORE_DATA))
    api(project(CoreModuleDependency.CORE_NETWORK))

    // Lifecycle
    implementation(LibraryDependency.LIVEDATA)

    // UI
    implementation(LibraryDependency.GLIDE)

    // Serialization
    kapt(LibraryDependency.MOSHI_ADAPTER)

    // Debugging
    implementation(LibraryDependency.TIMBER)

    // Database
    kapt(LibraryDependency.ROOM_COMPILER)

}

// Return value of config fields stored in `keys.properties`
fun getProperty(key: String): String {
    val properties = Properties()
    val file = file("apikey.properties")
    val inputStream = FileInputStream(file)
    properties.load(inputStream)

    return properties[key].toString()
}