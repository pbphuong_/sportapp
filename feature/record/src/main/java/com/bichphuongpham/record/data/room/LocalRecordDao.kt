package com.bichphuongpham.record.data.room

import androidx.room.Dao
import androidx.room.Query
import com.bichphuongpham.data.BaseDao
import com.bichphuongpham.record.data.model.LocalRecordEntity

@Dao
interface LocalRecordsDao : BaseDao<LocalRecordEntity> {

    @Query("SELECT * FROM record_table")
    suspend fun getAllLocalRecords(): List<LocalRecordEntity>
}