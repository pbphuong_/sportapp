package com.bichphuongpham.record.domain.usecase

import com.bichphuongpham.data.BaseUseCase
import com.bichphuongpham.record.domain.RecordsRepository

class GetAllRecordsUseCase(private val recordsRepo: RecordsRepository) : BaseUseCase() {

    suspend operator fun invoke() = recordsRepo.getAllRecords()
}