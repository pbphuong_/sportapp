package com.bichphuongpham.record.ui.adapter

import android.view.ViewGroup
import com.bichphuongpham.record.domain.model.IRecord
import com.bichphuongpham.record.domain.model.IRecordLocal
import com.bichphuongpham.record.domain.model.IRecordRemote
import com.bichphuongpham.record.domain.model.Storage
import com.bichphuongpham.record.ui.view.RecordItemView
import com.bichphuongpham.ui.BaseAdapter

/**
 * This adapter is for binding the sport records data with the RecordItemView.
 * Filter records according to the selected storage.
 */
class RecordsAdapter(
    private val records: MutableList<IRecord> = mutableListOf()
) : BaseAdapter<IRecord>(records) {

    private val filteredRecords = records.toMutableList()
    private var filter: Storage? = null

    override fun getItemCount() = filteredRecords.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RecordItemView(parent.context))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val records = filteredRecords[position]
        (holder.view as RecordItemView).setData(records)
    }

    /**
     * Filter records according to the selected storage.
     * If no storage is specified, show all the records.
     */
    fun filter(storage: Storage?) {
        val filteredRecords = when (storage) {
            is Storage.Local -> records.filterIsInstance<IRecordLocal>()
            is Storage.Remote -> records.filterIsInstance<IRecordRemote>()
            else -> records

        }
        setFilteredRecords(filteredRecords)
    }

    private fun setFilteredRecords(records: List<IRecord>) {
        filteredRecords.clear()
        filteredRecords.addAll(records)
        notifyDataSetChanged()
    }

    override fun setData(newData: MutableList<IRecord>) {
        data.clear()
        data.addAll(newData)
        filter(filter)
    }
}