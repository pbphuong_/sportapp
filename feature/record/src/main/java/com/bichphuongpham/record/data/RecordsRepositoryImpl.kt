package com.bichphuongpham.record.data

import com.bichphuongpham.data.ErrorType
import com.bichphuongpham.data.Result
import com.bichphuongpham.data.Result.*
import com.bichphuongpham.record.data.model.LocalRecordEntity
import com.bichphuongpham.record.data.room.LocalRecordsDao
import com.bichphuongpham.record.domain.RecordsRepository
import com.bichphuongpham.record.domain.model.IRecord
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class RecordsRepositoryImpl(
    private val localRecordsDao: LocalRecordsDao,
    private val remoteRecordsApi: RemoteRecordsApi
) : RecordsRepository {

    override suspend fun getAllRecords(): Result<List<IRecord>> = safeExecute {
        withContext(Dispatchers.IO) {
            // These will run in parallel, they are independent
            val remoteRecords = async { remoteRecordsApi.getRemoteRecords() }
            val localRecordsEntity = async { localRecordsDao.getAllLocalRecords() }

            // Suspend this block and wait for the result, then resume
            val localRecords = localRecordsEntity.await()
            val remoteResult = remoteRecords.await()

            when (remoteResult) {
                is Success -> {
                    // Combine local and remote records
                    val result = remoteResult.data.plus(localRecords)
                    when (result.isNotEmpty()) {
                        true -> Success(result)
                        false -> Failure(ErrorType.NoDataError)
                    }
                }
                is FailureWithData -> FailureWithData(localRecords, remoteResult.errorType)
                is Failure -> {
                    // There was an error form the server side but local records can still be passed
                    FailureWithData(localRecords, remoteResult.errorType)
                }
            }
        }
    }

    override suspend fun saveLocalRecord(record: IRecord) = safeExecute {
        with(record) {
            val entity = LocalRecordEntity(duration = duration, sportName = sportName, location = location)
            localRecordsDao.insert(entity)
            Success(Unit)
        }
    }

    override suspend fun saveRemoteRecord(record: IRecord) = safeExecute {
        remoteRecordsApi.saveRemoteRecord(record)
        Success(Unit)
    }
}