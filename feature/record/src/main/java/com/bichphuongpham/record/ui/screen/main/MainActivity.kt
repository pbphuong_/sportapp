package com.bichphuongpham.record.ui.screen.main

import com.bichphuongpham.record.R
import com.bichphuongpham.ui.lifecycle.BaseActivity

class MainActivity : BaseActivity() {

    override val layoutId = R.layout.activity_main
}