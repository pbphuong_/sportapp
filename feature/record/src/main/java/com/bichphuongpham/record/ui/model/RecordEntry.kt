package com.bichphuongpham.record.ui.model

import com.bichphuongpham.record.domain.model.IRecord

data class RecordEntry(
    override val sportName: String,
    override val location: String,
    override val duration: Long
) : IRecord