package com.bichphuongpham.record.ui

import androidx.lifecycle.MutableLiveData

/**
 * Format seconds to a string containing minutes and seconds.
 */
fun Long.secondsToFormattedTime(): String {
    val secondsInMinute = 60
    val minutes = (this / secondsInMinute).toInt()
    val seconds = this - minutes * secondsInMinute

    return when {
        minutes == 0 -> "${seconds}s"
        seconds == 0L -> "${minutes}min"
        else -> "${minutes}min ${seconds}s"
    }
}

inline fun <T : Any> wrapLoading(loadingValue: MutableLiveData<Boolean>, block: () -> T) {
    loadingValue.postValue(true)
    block()
    loadingValue.postValue(false)
}