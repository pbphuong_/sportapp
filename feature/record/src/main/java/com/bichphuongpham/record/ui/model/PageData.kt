package com.bichphuongpham.record.ui.model

data class PageData(
    val titleResource: Int,
    val iconResource: Int
)