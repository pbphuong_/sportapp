package com.bichphuongpham.record.data.di

import com.bichphuongpham.record.data.room.LocalRecordDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.module

val databaseModule: Module = module {

    // Database
    single { LocalRecordDatabase.buildDatabase(application = androidApplication()) }
    single { get<LocalRecordDatabase>().localRecordDao() }
}