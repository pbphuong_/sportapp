package com.bichphuongpham.record.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bichphuongpham.record.domain.model.IRecordLocal

@Entity(tableName = "record_table")
data class LocalRecordEntity(
    @PrimaryKey(autoGenerate = true)
    val recordId: Long = 0,
    override val sportName: String,
    override val duration: Long,
    override val location: String
) : IRecordLocal