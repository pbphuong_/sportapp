package com.bichphuongpham.record.ui.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bichphuongpham.record.ui.model.PageData
import com.bichphuongpham.record.ui.screen.form.RecordFormFragment
import com.bichphuongpham.record.ui.screen.listing.RecordsListingFragment

class PageAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val pages = mutableListOf<PageData>()

    override fun getItemCount() = pages.size

    override fun createFragment(position: Int) = when (position) {
        0 -> RecordFormFragment()
        1 -> RecordsListingFragment()
        else -> throw RuntimeException("Position out of pages bound")
    }

    fun setPages(data: List<PageData>) {
        pages.clear()
        pages.addAll(data)
    }

    fun getPageTitle(position: Int) = pages.getOrNull(position)?.titleResource ?: throw RuntimeException("Position out of pages bound")

    fun getPageIcon(position: Int) = pages.getOrNull(position)?.iconResource ?: throw RuntimeException("Position out of pages bound")
}