package com.bichphuongpham.record.data.retrofit

import com.bichphuongpham.data.Result
import com.bichphuongpham.data.Result.Success
import com.bichphuongpham.record.data.RemoteRecordsApi
import com.bichphuongpham.record.data.model.RemoteRecord
import com.bichphuongpham.record.data.safeExecute
import com.bichphuongpham.record.domain.model.IRecord

class RemoteRecordsApiImpl(private val remoteRecordsService: RemoteRecordsApiService) : RemoteRecordsApi {

    override suspend fun getRemoteRecords(): Result<List<IRecord>> = safeExecute {
        val records = try {
            remoteRecordsService.getRecords().values.toList()
        } catch (exception: KotlinNullPointerException) {
            // This could be handled differently
            // Could not find solution on this issue
            emptyList<IRecord>()
        }
        Success(records)
    }

    override suspend fun saveRemoteRecord(record: IRecord) {
        with(record) {
            val request = RemoteRecord(sportName = sportName, duration = duration, location = location)
            remoteRecordsService.saveRecord(request)
        }
    }
}