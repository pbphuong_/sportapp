package com.bichphuongpham.record.data

import com.bichphuongpham.data.Result
import com.bichphuongpham.record.domain.model.IRecord

interface RemoteRecordsApi {

    suspend fun getRemoteRecords(): Result<List<IRecord>>

    suspend fun saveRemoteRecord(record: IRecord)
}