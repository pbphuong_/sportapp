package com.bichphuongpham.record.domain

import com.bichphuongpham.data.Result
import com.bichphuongpham.record.domain.model.IRecord

interface RecordsRepository {

    suspend fun getAllRecords(): Result<List<IRecord>>

    suspend fun saveLocalRecord(record: IRecord): Result<Unit>

    suspend fun saveRemoteRecord(record: IRecord): Result<Unit>
}