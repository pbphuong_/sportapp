package com.bichphuongpham.record.ui.screen.main

import com.bichphuongpham.record.R
import com.bichphuongpham.record.ui.model.PageData
import com.bichphuongpham.ui.lifecycle.BaseViewModel

class MainViewModel : BaseViewModel() {

    private val pageRecord = PageData(R.string.tab_title_form, R.drawable.ic_record)
    private val pageListing = PageData(R.string.tab_title_listing, R.drawable.ic_listing)

    val pages = listOf(pageRecord, pageListing)
}