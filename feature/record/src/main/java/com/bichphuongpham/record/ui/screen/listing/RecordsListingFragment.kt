package com.bichphuongpham.record.ui.screen.listing

import com.bichphuongpham.data.ErrorType
import com.bichphuongpham.record.R
import com.bichphuongpham.record.databinding.FragmentRecordsListingBindingImpl
import com.bichphuongpham.record.ui.RecordSharedViewModel
import com.bichphuongpham.record.ui.adapter.RecordsAdapter
import com.bichphuongpham.ui.lifecycle.BaseFragment
import com.bichphuongpham.ui.utils.observe
import com.bichphuongpham.ui.utils.showSnackbar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_records_listing.*
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * This screen shows the records listing. User can filter out only local or remote entries.
 */
class RecordsListingFragment : BaseFragment<RecordsListingViewModel, FragmentRecordsListingBindingImpl>() {

    // To only listen to update events
    private val sharedViewModel: RecordSharedViewModel by viewModel()

    override val layoutId = R.layout.fragment_records_listing

    override fun initViewModel() {
        viewModel = getViewModel()
    }

    override fun initBindingDataSources() {
        binding.vm = viewModel
    }

    override fun initUi() {
        setupListing()
    }

    override fun onResume() {
        super.onResume()
        showStoredErrorMessages()
    }

    override fun initObservables() {
        // Listen to new submissions and update the listing
        sharedViewModel.onDataUpdated.observe(this) {
            viewModel.loadListing()
        }
    }

    private fun showStoredErrorMessages() {
        // Show a snackbar with a corresponding error message
        viewModel.errorType.value?.let {
            val textResId = when (it) {
                ErrorType.NetworkConnectionError -> R.string.error_connection_listing_description
                ErrorType.NoDataError -> R.string.error_empty_data_description
                ErrorType.ServerError -> R.string.error_general_description
            }
            showSnackbar(vContainer, textResId, Snackbar.LENGTH_LONG)
            viewModel.onErrorMessageShowed()
        }
    }

    private fun setupListing() {
        val adapter = RecordsAdapter()
        vRecordsListing.adapter = adapter

        viewModel.storageFilter.observe(this) {
            Timber.d("Checked storage filter $it")
            adapter.filter(it)
        }
    }
}