package com.bichphuongpham.record.data.retrofit

import com.bichphuongpham.record.data.model.RemoteRecord
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Holds the API calls for the Sport records app.
 */
interface RemoteRecordsApiService {

    @GET("/records.json")
    suspend fun getRecords(): Map<String, RemoteRecord>

    @POST("/records.json")
    suspend fun saveRecord(@Body record: RemoteRecord)
}