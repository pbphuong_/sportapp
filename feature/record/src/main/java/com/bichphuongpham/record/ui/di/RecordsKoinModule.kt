package com.bichphuongpham.record.ui.di

import com.bichphuongpham.record.domain.usecase.GetAllRecordsUseCase
import com.bichphuongpham.record.domain.usecase.SaveLocalRecordUseCase
import com.bichphuongpham.record.domain.usecase.SaveRemoteRecordUseCase
import com.bichphuongpham.record.ui.RecordSharedViewModel
import com.bichphuongpham.record.ui.screen.form.RecordFormViewModel
import com.bichphuongpham.record.ui.screen.listing.RecordsListingViewModel
import com.bichphuongpham.record.ui.screen.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val sportRecordsModule: Module = module {

    // Use cases
    single { SaveLocalRecordUseCase(recordsRepo = get()) }
    single { SaveRemoteRecordUseCase(recordsRepo = get()) }
    single { GetAllRecordsUseCase(recordsRepo = get()) }

    // ViewModels
    single { RecordSharedViewModel() }
    viewModel { MainViewModel() }
    viewModel { RecordFormViewModel(saveLocalRecordUseCase = get(), saveRemoteRecordUseCase = get()) }
    viewModel { RecordsListingViewModel(getAllRecordsUseCase = get()) }
}