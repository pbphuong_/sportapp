package com.bichphuongpham.record.domain.model

interface IRecord {
    val sportName: String
    val location: String
    val duration: Long
}