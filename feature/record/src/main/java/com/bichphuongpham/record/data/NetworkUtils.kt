package com.bichphuongpham.record.data

import com.bichphuongpham.data.ErrorType
import com.bichphuongpham.data.Result
import com.bichphuongpham.network.exception.NoInternetConnectionException
import timber.log.Timber

/**
 * Extension on functions that returns data in a Result wrapper.
 * Handles exceptions and passes corresponding error types.
 */
inline fun <T : Any> safeExecute(block: () -> Result<T>): Result<T> = try {
    block()
} catch (exception: NoInternetConnectionException) {
    Timber.e(exception.localizedMessage)
    Result.Failure(ErrorType.NetworkConnectionError)

} catch (exception: Exception) {
    Timber.e(exception.localizedMessage)
    Result.Failure(ErrorType.ServerError)
}