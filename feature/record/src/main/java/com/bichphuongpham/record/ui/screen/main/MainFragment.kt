package com.bichphuongpham.record.ui.screen.main

import com.bichphuongpham.record.R
import com.bichphuongpham.record.databinding.FragmentMainBindingImpl
import com.bichphuongpham.record.ui.adapter.PageAdapter
import com.bichphuongpham.ui.lifecycle.BaseFragment
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.getViewModel

class MainFragment : BaseFragment<MainViewModel, FragmentMainBindingImpl>() {

    override val layoutId = R.layout.fragment_main

    override fun initViewModel() {
        viewModel = getViewModel()
    }

    override fun initBindingDataSources() {}

    override fun initUi() {
        setupViewPager()
    }

    private fun setupViewPager() {
        val adapter = PageAdapter(this)
        adapter.setPages(viewModel.pages)
        vViewPager.adapter = adapter

        // Set tab items with appropriate page titles and icons
        TabLayoutMediator(vTabLayout, vViewPager) { tab, position ->
            context?.apply {
                tab.text = getString(adapter.getPageTitle(position))
                tab.icon = getDrawable(adapter.getPageIcon(position))
            }
        }.attach()
    }
}