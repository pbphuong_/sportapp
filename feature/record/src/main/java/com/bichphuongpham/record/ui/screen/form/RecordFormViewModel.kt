package com.bichphuongpham.record.ui.screen.form

import androidx.lifecycle.MutableLiveData
import com.bichphuongpham.data.ErrorType
import com.bichphuongpham.record.domain.model.Storage
import com.bichphuongpham.record.domain.usecase.SaveLocalRecordUseCase
import com.bichphuongpham.record.domain.usecase.SaveRemoteRecordUseCase
import com.bichphuongpham.record.ui.model.RecordEntry
import com.bichphuongpham.record.ui.wrapLoading
import com.bichphuongpham.ui.lifecycle.BaseViewModel
import com.bichphuongpham.ui.lifecycle.MutableLiveDataNonNull
import com.bichphuongpham.ui.lifecycle.ObservableFieldNonNull
import com.bichphuongpham.ui.lifecycle.SingleLiveEvent
import kotlinx.coroutines.delay
import timber.log.Timber

class RecordFormViewModel(
    private val saveLocalRecordUseCase: SaveLocalRecordUseCase,
    private val saveRemoteRecordUseCase: SaveRemoteRecordUseCase
) :
    BaseViewModel() {

    // UI States
    val onEntrySubmittedEvent = SingleLiveEvent<Void>()
    val onEntrySportNameBlankEvent = SingleLiveEvent<Void>()
    val onEntryLocationBlankEvent = SingleLiveEvent<Void>()

    val isDataLoading = MutableLiveData(false)
    val errorType = MutableLiveData<ErrorType?>()

    // Form inputs
    private var selectedStorage: Storage = Storage.Local
    val entrySportName = ObservableFieldNonNull("")
    val entryLocation = ObservableFieldNonNull("")
    var entryDurationMinutes = MutableLiveDataNonNull(0)
    var entryDurationSeconds = MutableLiveDataNonNull(0)

    private val entryDuration: Long
        get() = entryDurationMinutes.value * SECONDS_IN_MINUTE + entryDurationSeconds.value

    fun onLocalStorageSelected() {
        selectedStorage = Storage.Local
    }

    fun onRemoteStorageSelected() {
        selectedStorage = Storage.Remote
    }

    fun onDurationMinutesValueChanged(duration: Int) {
        entryDurationMinutes.postValue(duration)
    }

    fun onDurationSecondsValueChanged(duration: Int) {
        entryDurationSeconds.postValue(duration)
    }

    fun submitEntry() {
        if (!isEntryValid()) return

        val entry = RecordEntry(duration = entryDuration, sportName = entrySportName.get(), location = entryLocation.get())
        Timber.d("Submitting entry $entry")

        launch {
            wrapLoading(isDataLoading) {
                val result = when (selectedStorage) {
                    is Storage.Local -> saveLocalRecordUseCase(entry)
                    is Storage.Remote -> saveRemoteRecordUseCase(entry)
                }
                delay(300)      // Artificial delay for loading
                result.successOrError(::handleSaveEntrySuccess, ::handleSaveEntryFailure)
            }
        }
    }

    /**
     * Entry is valid when the string inputs are not blank.
     */
    private fun isEntryValid(): Boolean {
        var isValid = true

        if (entrySportName.get().isBlank()) {
            onEntrySportNameBlankEvent.call()
            isValid = false
        }
        if (entryLocation.get().isBlank()) {
            onEntryLocationBlankEvent.call()
            isValid = false
        }

        return isValid
    }

    private fun handleSaveEntrySuccess(p: Unit) {
        Timber.d("On entry saved call")
        errorType.postValue(null)
        onEntrySubmittedEvent.call()
    }

    private fun handleSaveEntryFailure(error: ErrorType) {
        Timber.d("Saving entry failed on: $error")
        errorType.postValue(error)
    }

    companion object {
        const val SECONDS_IN_MINUTE = 60L
    }
}