package com.bichphuongpham.record.data.di

import com.bichphuongpham.network.NetworkHandler
import com.bichphuongpham.network.RetrofitBuilder
import com.bichphuongpham.network.interceptors.NetworkInterceptor
import com.bichphuongpham.record.BuildConfig
import com.bichphuongpham.record.data.RecordsRepositoryImpl
import com.bichphuongpham.record.data.RemoteRecordsApi
import com.bichphuongpham.record.data.retrofit.RemoteRecordsApiImpl
import com.bichphuongpham.record.data.retrofit.RemoteRecordsApiService
import com.bichphuongpham.record.domain.RecordsRepository
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit

val networkModule: Module = module {

    with(RetrofitBuilder) {
        single { NetworkHandler(context = get()) }
        single { NetworkInterceptor(connectionHandler = get()) }

        single { buildClient(interceptors = listOf(get<NetworkInterceptor>())) }
        single { buildRetrofit(baseUrl = BuildConfig.BASE_URL, client = get()) }
        single { provideRemoteRecordsApi(retrofit = get()) }
    }

    single<RemoteRecordsApi> { RemoteRecordsApiImpl(remoteRecordsService = get()) }
    single<RecordsRepository> { RecordsRepositoryImpl(remoteRecordsApi = get(), localRecordsDao = get()) }
}

fun provideRemoteRecordsApi(retrofit: Retrofit): RemoteRecordsApiService {
    return retrofit.create(RemoteRecordsApiService::class.java)
}