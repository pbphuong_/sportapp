package com.bichphuongpham.record.domain.usecase

import com.bichphuongpham.data.BaseUseCase
import com.bichphuongpham.record.domain.RecordsRepository
import com.bichphuongpham.record.domain.model.IRecord

class SaveLocalRecordUseCase(private val recordsRepo: RecordsRepository) : BaseUseCase() {

    suspend operator fun invoke(record: IRecord) = recordsRepo.saveLocalRecord(record)
}