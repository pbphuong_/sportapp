package com.bichphuongpham.record.ui.screen.listing

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.bichphuongpham.data.ErrorType
import com.bichphuongpham.record.domain.model.IRecord
import com.bichphuongpham.record.domain.model.Storage
import com.bichphuongpham.record.domain.usecase.GetAllRecordsUseCase
import com.bichphuongpham.record.ui.wrapLoading
import com.bichphuongpham.ui.lifecycle.BaseViewModel
import timber.log.Timber

class RecordsListingViewModel(private val getAllRecordsUseCase: GetAllRecordsUseCase) : BaseViewModel() {

    // UI states
    val isDataLoading = MutableLiveData(false)
    val errorType = MutableLiveData<ErrorType?>()

    val recordsListings = MutableLiveData<List<IRecord>>()

    // Filter according to storage
    val storageFilter = MediatorLiveData<Storage?>()
    private val isLocalFilterChecked = MutableLiveData(true)
    private val isRemoteFilterChecked = MutableLiveData(true)

    init {
        loadListing()
        setupFilter()
    }

    fun loadListing() {
        Timber.d("Loading records listing..")

        launch {
            wrapLoading(isDataLoading) {
                val result = getAllRecordsUseCase()
                result.successOrError(::handleSuccess, ::handleFailure)
            }
        }
    }

    private fun handleSuccess(data: List<IRecord>) {
        errorType.postValue(null)
        recordsListings.postValue(data)
    }

    private fun handleFailure(error: ErrorType) {
        Timber.d("Get listing failed on: $error")
        errorType.postValue(error)
    }

    fun onErrorMessageShowed() {
        errorType.postValue(null)
    }

    private fun setupFilter() {
        storageFilter.addSource(isLocalFilterChecked) { isChecked ->
            changeStorageFilterOn(isChecked, Storage.Local)
        }
        storageFilter.addSource(isRemoteFilterChecked) { isChecked ->
            changeStorageFilterOn(isChecked, Storage.Remote)
        }
    }

    private fun changeStorageFilterOn(isFilterChecked: Boolean, filterStorage: Storage) {
        // The same filter is checked again
        if (isFilterChecked && storageFilter.value == filterStorage) return

        // Switch filters
        val storage = when (isFilterChecked) {
            false -> filterStorage.getOppositeStorage()
            true -> null
        }
        storageFilter.postValue(storage)
    }

    fun onLocalCheckedChange(isChecked: Boolean) {
        isLocalFilterChecked.postValue(isChecked)
    }

    fun onRemoteCheckedChange(isChecked: Boolean) {
        isRemoteFilterChecked.postValue(isChecked)
    }
}