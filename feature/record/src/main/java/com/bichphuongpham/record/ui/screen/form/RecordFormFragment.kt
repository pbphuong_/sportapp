package com.bichphuongpham.record.ui.screen.form

import com.bichphuongpham.data.ErrorType
import com.bichphuongpham.record.R
import com.bichphuongpham.record.databinding.FragmentRecordFormBindingImpl
import com.bichphuongpham.record.ui.RecordSharedViewModel
import com.bichphuongpham.ui.lifecycle.BaseFragment
import com.bichphuongpham.ui.utils.observe
import com.bichphuongpham.ui.utils.removeDefaultFilters
import com.bichphuongpham.ui.utils.showSnackbar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_record_form.*
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * This screen shows the form to fill out the sport record information such as a sport name, location and a duration.
 * User has an option to choose where to save the record entry - to local or remote storage.
 */
class RecordFormFragment : BaseFragment<RecordFormViewModel, FragmentRecordFormBindingImpl>() {

    // To only trigger the update events
    private val sharedViewModel: RecordSharedViewModel by viewModel()

    override val layoutId = R.layout.fragment_record_form

    override fun initViewModel() {
        viewModel = getViewModel()
    }

    override fun initBindingDataSources() {
        binding.vm = viewModel
    }

    override fun initUi() {
        setupDuration()
    }

    override fun initObservables() {
        // Notify ListingFragment to update data on a new submission
        viewModel.onEntrySubmittedEvent.observe(this) {
            showSnackbar(vSnackbarContainer, R.string.form_saved_success, Snackbar.LENGTH_SHORT)
            sharedViewModel.onUpdate()
        }

        // Show error message on empty location
        viewModel.onEntryLocationBlankEvent.observe(this) {
            vLocation.error = getString(R.string.form_error_blank_location)
        }

        // Show error message on empty sport name
        viewModel.onEntrySportNameBlankEvent.observe(this) {
            vSportName.error = getString(R.string.form_error_blank_sport_name)
        }

        // Show a snackbar with a corresponding error message
        viewModel.errorType.observe(this) { error ->
            error?.let {
                val textResId = when (it) {
                    ErrorType.NetworkConnectionError -> R.string.error_connection_save_description
                    else -> R.string.error_general_description
                }
                showSnackbar(vSnackbarContainer, textResId, Snackbar.LENGTH_LONG)
            }
        }
    }

    private fun setupDuration() {
        vMinutes.apply {
            minValue = MIN_TIME_VALUE
            maxValue = MAX_TIME_VALUE
            value = viewModel.entryDurationMinutes.value
            setFormatter { getString(R.string.form_minutes, it) }
            removeDefaultFilters()      // To format the default value
        }

        vSeconds.apply {
            minValue = MIN_TIME_VALUE
            maxValue = MAX_TIME_VALUE
            value = viewModel.entryDurationSeconds.value
            setFormatter { getString(R.string.form_seconds, it) }
            removeDefaultFilters()
        }
    }

    companion object {
        const val MIN_TIME_VALUE = 0
        const val MAX_TIME_VALUE = 59   // Max value of minutes or seconds
    }
}