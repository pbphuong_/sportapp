package com.bichphuongpham.record.data.model

import com.bichphuongpham.record.domain.model.IRecordRemote
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RemoteRecord(
    override val sportName: String,
    override val location: String,
    override val duration: Long
) : IRecordRemote