package com.bichphuongpham.record.data.room

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.bichphuongpham.record.data.model.LocalRecordEntity

@Database(entities = [LocalRecordEntity::class], version = 1, exportSchema = false)
abstract class LocalRecordDatabase : RoomDatabase() {

    abstract fun localRecordDao(): LocalRecordsDao

    companion object {
        private const val DATABASE_NAME = "LocalRecordsDatabase"

        fun buildDatabase(application: Application) =
            Room.databaseBuilder(application, LocalRecordDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }
}