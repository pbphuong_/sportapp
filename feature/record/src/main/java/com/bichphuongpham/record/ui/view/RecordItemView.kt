package com.bichphuongpham.record.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.FrameLayout
import com.bichphuongpham.record.R
import com.bichphuongpham.record.domain.model.IRecord
import com.bichphuongpham.record.domain.model.IRecordLocal
import com.bichphuongpham.record.domain.model.IRecordRemote
import com.bichphuongpham.record.ui.secondsToFormattedTime
import com.bichphuongpham.ui.utils.inflateCustomViewContent
import kotlinx.android.synthetic.main.view_record_item.view.*

/**
 * This class represents a single item in the records listing.
 * It shows an colored label representing the storage, sport name, location and a record duration.
 */
class RecordItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    FrameLayout(context, attrs, defStyleAttr) {

    init {
        inflateCustomViewContent(R.layout.view_record_item)
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    fun setData(record: IRecord) {
        vTitle.text = record.sportName
        vLocation.text = record.location
        vDuration.text = record.duration.secondsToFormattedTime()

        // Show colored label according to record storage
        val color = when (record) {
            is IRecordLocal -> R.color.orange
            is IRecordRemote -> R.color.blue
            else -> throw RuntimeException("Type of record not recognized")
        }
        vStorageColorLabel.setBackgroundResource(color)
    }
}