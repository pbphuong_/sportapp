package com.bichphuongpham.record.ui

import com.bichphuongpham.ui.lifecycle.BaseViewModel
import com.bichphuongpham.ui.lifecycle.SingleLiveEvent

/**
 * A shared viewModel to communicate the update events between the fragments.
 */
class RecordSharedViewModel : BaseViewModel() {

    val onDataUpdated = SingleLiveEvent<Void>()

    fun onUpdate() {
        onDataUpdated.call()
    }
}