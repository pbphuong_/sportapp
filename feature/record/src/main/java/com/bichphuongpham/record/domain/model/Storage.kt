package com.bichphuongpham.record.domain.model

sealed class Storage {

    object Local : Storage() {
        override fun getOppositeStorage() = Remote
    }

    object Remote : Storage() {
        override fun getOppositeStorage() = Local
    }

    abstract fun getOppositeStorage(): Storage
}