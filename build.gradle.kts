// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        google()
        jcenter()
        maven(url = MavenRepository.FABRIC)
    }
    dependencies {
        classpath(LibraryDependency.GRADLE)
        classpath(LibraryDependency.GRADLE_KOTLIN)
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven { setUrl(MavenRepository.JITPACK) }
        mavenLocal()
        mavenCentral()
    }
}

task("clean") {
    delete(rootProject.buildDir)
}